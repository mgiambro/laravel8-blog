<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Support\Facades\Gate;

class PostController extends Controller
{
    public function index()
    {

        //    ddd(Gate::allows('admin'));

        // ddd(request()->user()->can('admin'));

        //   $this->authorize('admin');

        return view('posts.index', [
            'posts' => Post::latest()->filter(request(['search', 'category', 'author']))->paginate(6)->withQueryString()
        ]);
    }

    public function show(Post $post)
    {
        return view('posts.show', [
            'post' => $post
        ]);
    }

    // UNUSED EXAMPLE CODE

    // DB::listen(function($query){
    //     logger($query->sql);
    // });í

}
