<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class SessionsController extends Controller
{

    public function store()
    {

        // validate the erequest
        $attributes = request()->validate([
            'email' => 'required|exists:users,email',
            'password' => 'required'
        ]);

        // attempt to authenticate and log in the user
        if (!auth()->attempt($attributes)) {
            // auth failed
            throw ValidationException::withMessages(['email' => 'Your provided credentials could not be verified.']);
        }

        session()->regenerate();

        return redirect('/')->with('success', 'Welcome Back!');

        // return back()
        //     ->withInput()
        //     ->withErrors(['email' => 'Your provided credentials could not be verified.']);
    }

    public function create()
    {
        return view('sessions.create');
    }

    public function destroy()
    {
        auth()->logout();

        return redirect('/')->with('success', 'Goodbye!');
    }
}
