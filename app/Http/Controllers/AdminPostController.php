<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Validation\Rule;


class AdminPostController extends Controller
{
    public function index()
    {

        return view('admin.posts.index', [
            'posts' => Post::paginate(50)
        ]);
    }

    public function create()
    {
        return view('admin.posts.create');
    }

    protected function validatePost(?Post $post = null)
    {
        $post = isset($post) === true ? $post : new Post();

        return request()->validate([
            'title' => 'required',
            'thumbnail' => $post->exists ? ['imaage'] : ['required|image'],
            'slug' => ['required', Rule::unique('posts', 'slug')->ignore($post->id)],
            'excerpt' => 'required',
            'body' => 'required',
            'category_id' => ['required', Rule::exists('categories', 'id')]
        ]);
    }

    public function store()
    {

        // $attributes = $this->validatePost();
        // $attributes['user_id'] = auth()->id();
        // $path = $attributes['thumbnail'] = request()->file('thumbnail')->store('thumbnails');

        $attributes = array_merge(
            $this->validatePost(),
            [
                'user_id' => $attributes['user_id'] = auth()->id(),
                'thumbnail' => request()->file('thumbnail')->store('thumbnails')
            ]
        );

        Post::create($attributes);

        return redirect('/');
    }

    public function edit(Post $post)
    {
        return view('admin.posts.edit', ['post' => $post]);
    }

    public function update(Post $post)
    {

        $attributes = $this->validatePost($post);

        if (isset($attributes['thumbnail'])) {
            $attributes['thumbnail'] = request()->file('thumbnail')->store('thumbnails');
        }

        $post->update($attributes);

        return back()->with('success', 'Post Updated!');
    }

    public function destroy(Post $post)
    {
        $post->delete();

        return back()->with('success', 'Post Deleted!');
    }
}
