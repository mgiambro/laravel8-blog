<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MustBeAdministrator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */


    // Not needed as we can use Gate instead
    // public function handle(Request $request, Closure $next)
    // {

    //     if (auth()->user()) {
    //         if (auth()->user()->username !== 'mgiambro') {
    //             abort(Response::HTTP_FORBIDDEN);
    //         }
    //     }

    //     return $next($request);
    // }
}
