<?php

namespace App\Models;

use App\Models\User;
use App\Models\Comment;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

  use HasFactory;

  /**
   * Loads category and author with post to avoid n+1. Saves specifying in routes file.
   * If there's a case for not needing category and author, you could use $post->without('category','author') in a route.
   */

  protected $with = ['category', 'author'];

  // Not needed. Set globally in AppServiceProvider
  protected $_guarded = []; // What you cannot mass-assign. Not needed. Set globally in AppServiceProvider
  //  protected $fillable = ['title'];    // What you can mass-assgign
  //

  public function scopeFilter($query, array $filters)
  {
    if (isset($filters['search'])) {
      $query->where(function ($query) {
        $query
          ->where('title', 'like', '%' . request('search') . '%')
          ->orWhere('body', 'like', '%' . request('search') . '%');
      });
    }

    if (isset($filters['category'])) {
      $category = $filters['category'];
      $query
        ->whereHas(
          'category',
          function ($query) use ($category) {
            $query->where('slug', $category);
          }
        );
    }
    // if (isset($filters['category'])) {
    //   $category = $filters['category'];
    //   $query
    //     ->whereExists(
    //       function ($query) use ($category) {
    //         $query->from('categories')
    //           ->whereColumn('categories.id', 'posts.category_id')
    //           ->where('categories.slug', $category);
    //       }
    //     );
    // }

    if (isset($filters['author'])) {
      $author = $filters['author'];
      $query
        ->whereHas(
          'author',
          function ($query) use ($author) {
            $query->where('username', $author);
          }
        );
    }


    // OR

    // $query->when(isset($filters['search']), function($query, $search){
    //     $query
    //     ->where('title', 'like', '%' . $search. '%')
    //     ->orWhere('body', 'like', '%' . $search . '%');
    // });
  }

  public function category()
  {
    return $this->belongsTo(Category::class);
  }

  public function author()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  // Specify which property on an object the route should use as a key
  // public function getRouteKeyName()
  // {
  //     return 'slug';
  // }

  public function comments()
  {
    return $this->hasMany(Comment::class);
  }
}
