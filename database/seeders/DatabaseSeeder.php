<?php

namespace Database\Seeders;

use App\Models\Post;
use App\Models\Category;
use Illuminate\Database\Seeder;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        // User::truncate();
        // Category::truncate();
        // Post::truncate();

        // To specify a user name instead of using fake data
        // $user = User::factory()->create([
        //     'name' => 'John Doe'
        // ]);

        // Post::factory(10)->create([
        //     'user_id' => $user->id
        // ]);

       Post::factory(30)->create();

    }
}
