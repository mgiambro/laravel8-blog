@extends('layout')

@section('content')
<body>
    <h1>{{ $post->title }}</h1>
    <div>
      {!! $post->body !!} 
</div>

    <a href="/laravel/blog/public/">Go Back</a>
</body>
@endsection