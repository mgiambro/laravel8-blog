<x-layout>

    <x-setting heading="Publish New Post">

        <form method="POST" action="/laravel/blog/public/admin/posts" enctype="multipart/form-data">

            @csrf

            <div class="mb-6">
                <x-form.input name="title" />
                <x-form.input name="slug" />
                <x-form.input name="thumbnail" type="file" />
                <x-form.textarea name="excerpt" />
                <x-form.textarea name="body" />

                <!-- Category -->
                <x-form.field>
                    <x-form.label name="category" />
                    <select class="" id="category_id" name="category_id">
                        @php
                        $categories = \App\Models\Category::all();
                        @endphp

                        @foreach ($categories as $category)
                        <option value="{{ $category->id }}" {{ old('category_id') == $category->id ? 'selected' : '' }}>
                            {{ ucwords($category->name) }}
                        </option>
                        @endforeach

                    </select>

                    <x-form.error name="category" />
                </x-form.field>

            </div>

            <x-form.button>Publish</x-form.button>
        </form>

    </x-setting>

</x-layout>
