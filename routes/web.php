<?php

use App\Http\Controllers\NewsletterController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\SessionsController;
use App\Http\Controllers\PostCommentsController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminPostController;

// use App\Models\Post;
// use App\Models\Category;
// use App\Models\User;

Route::get('/', [PostController::class, 'index'])->name('home');

// Bind to slug
Route::get('posts/{post:slug}', [PostController::class, 'show']);

Route::post('newsletter', NewsletterController::class);

Route::get('register', [RegisterController::class, 'create'])->middleware('guest');
Route::post('register', [RegisterController::class, 'store'])->middleware('guest');
Route::post('register', [RegisterController::class, 'store']);
Route::post('logout', [SessionsController::class, 'destroy'])->middleware('auth');
Route::get('login', [SessionsController::class, 'create'])->middleware('guest');
Route::post('sessions', [SessionsController::class, 'store']);

Route::post('posts/{post:slug}/comments', [PostCommentsController::class, 'store']);

// Admin
Route::middleware('can:admin')->group(function () {
    Route::resource('admin/posts', AdminPostController::class)->except('show');
    // Route::get('admin/posts/create', [AdminPostController::class, 'create']);
    // Route::get('admin/posts', [AdminPostController::class, 'index']);
    // Route::post('admin/posts', [AdminPostController::class, 'store']);
    // Route::get('admin/posts/{post}/edit', [AdminPostController::class, 'edit']);
    // Route::patch('admin/posts/{post}', [AdminPostController::class, 'update']);
    // Route::delete('admin/posts/{post}', [AdminPostController::class, 'destroy']);
});
// Route::get('admin/posts/create', [AdminPostController::class, 'create'])->middleware('can:admin');
// Route::get('admin/posts', [AdminPostController::class, 'index'])->middleware('can:admin');
// Route::post('admin/posts', [AdminPostController::class, 'store'])->middleware('can:admin');
// Route::get('admin/posts/{post}/edit', [AdminPostController::class, 'edit'])->middleware('can:admin');
// Route::patch('admin/posts/{post}', [AdminPostController::class, 'update'])->middleware('can:admin');
// Route::delete('admin/posts/{post}', [AdminPostController::class, 'destroy'])->middleware('can:admin');


// Route::get('categories/{category:slug}', function (Category $category) {
//     return view('posts', [
//         //  'posts' => $category->posts->load(['category','author']),
//         'posts' => $category->posts,
//         'currentCategory' => $category,
//         'categories' => Category::all()
//     ]);
// })->name('category');

// Route::get('authors/{author:username}', function (User $author) {
//     return view('posts.index', [
//         'posts' => $author->posts->load(['category', 'author'])
//     ]);
// });


// ################### UNUSED ###################

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('posts/{post}', function ($id) {

//     return view('post', [
//         'post' => Post::findOrFail($id)
//     ]);

// });

// Route Model Binding
// Route::get('posts/{post}', function (Post $post) {

//     return view('post', [
//         'post' => $post
//     ]);

// });
